Forward Proxy Server
====================

A small, simple and fast forward proxy
built on Node.js to replace Squid, Apache, etc.

Scripts
-------

* `./restart.sh` - start/restart the server.
* `./stop.sh` - stop the server.
* `./clean.sh` - clean the server after an unexpected shutdown.
* `./rotate.sh` - clean old logs.

Configuration
-------------

`config.js` contains the configuration. See `example-config.js` for details.

See Also
--------

* [JP](https://gitlab.com/aimnadze/jp) for log file processing.
* [Reverse Proxy Server](https://gitlab.com/aimnadze/reverse-proxy-server)
