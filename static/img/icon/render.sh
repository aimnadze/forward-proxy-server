#!/bin/bash
cd `dirname $BASH_SOURCE`
pwd
for i in *.svg
do
    echo "    $i"
    name=`basename $i .svg`
    inkscape --export-filename=$name.png $name.svg > /dev/null
    optipng -quiet -o7 -strip all $name.png
done
