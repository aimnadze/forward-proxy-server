<?php

function render_inkscape_svg_files ($argv) {

    echo getcwd() . "\n";

    $globs = array_slice($argv, 1);
    if (!$globs) $globs = ['*.svg'];

    foreach ($globs as $glob) {
        foreach (glob($glob) as $file) {
            echo "    $file\n";
            system("inkscape --vacuum-defs $file");
            system("inkscape --export-overwrite -l -o ../$file $file");
        }
    }

}
