exports.port = 3128

// Examples:
// exports.host = ''            // bind to all interfaces
// exports.host = '0.0.0.0'     // bind to all ipv4 interfaces
// exports.host = '::'          // bind to all ipv6 interfaces
// exports.host = '<ipAddress>' // bind to a specific ip address
exports.host = ''

exports.timeout = 60000

// Example:
// exports.https = {
//     key: 'ssl/key',
//     cert: 'ssl/cert',
//     ca: [
//         'ssl/ca1',
//         'ssl/ca2',
//         ..
//     ],
// }
