module.exports = {

    listen: {

        // Port to listen to
        port: 3128,

        // Host to listen to
        //
        // Examples:
        // host: '',            // bind to all interfaces
        // host: '0.0.0.0',     // bind to all ipv4 interfaces
        // host: '::',          // bind to all ipv6 interfaces
        // host: '<ipAddress>', // bind to a specific ip address
        host: '',

    },

    // Proxy request timeout
    timeout: 60000,

    // Listen to HTTPS instead of HTTP
    //
    // Example:
    // https: [
    //     {
    //         key: 'ssl/key',
    //         cert: 'ssl/cert',
    //         ca: [
    //             'ssl/ca1',
    //             'ssl/ca2',
    //         ],
    //     },
    //     // ...
    // ],

}
