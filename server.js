var cluster = require('cluster')

process.chdir(__dirname)

if (cluster.isMaster) require('./lib/Master.js')()
else require('./lib/Worker.js')()
