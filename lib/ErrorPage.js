var http = require('http')

module.exports = statusCode => {

    var title = statusCode + ' ' + http.STATUS_CODES[statusCode]

    var html =
        '<!DOCTYPE html>' +
        '<html>' +
            '<head>' +
                '<title>' + title + '</title>' +
                '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />' +
                '<meta name="viewport" content="width=device-width, user-scalable=no" />' +
            '</head>' +
            '<body>' +
                '<h1>' + title + '</h1>' +
            '</body>' +
        '</html>'

    return html

}
