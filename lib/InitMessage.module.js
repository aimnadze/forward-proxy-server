module.exports = app => () => {

    const timeout = app.config.timeout || 60000

    return {
        timeout,
        httpsOptions: app.HttpsOptions(),
    }

}
