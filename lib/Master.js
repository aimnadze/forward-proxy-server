module.exports = () => {

    var LogFiles = require('./LogFiles.js'),
        InitMessage = require('./InitMessage.js')

    var logsDir = 'logs',
        logFiles = LogFiles(logsDir),
        initMessage = InitMessage(logsDir)

    var TestServer = require('./TestServer.js')
    TestServer(initMessage.httpsOptions, initMessage.port, initMessage.host, () => {

        function fork () {

            var worker = cluster.fork()
            worker.on('online', () => {
                worker.on('disconnect', () => {
                    if (workers[id]) forkAlternative(id)
                })
                worker.send(initMessage)
            })
            worker.on('message', logFiles.add)

            var id = worker.id
            workers[id] = worker

        }

        function forkAlternative (id) {
            delete workers[id]
            fork()
        }

        function shutdown () {
            require('fs').unlinkSync('server.sock')
            process.exit(0)
        }

        var cluster = require('cluster'),
            net = require('net'),
            os = require('os')

        var workers = Object.create(null)

        var numCpus = os.cpus().length
        for (var i = 0; i < numCpus; i++) fork()

        net.createServer(c => {
            c.end()
            delete require.cache[require.resolve('../config.js')]
            initMessage = InitMessage(logsDir)
            for (var i in workers) {
                workers[i].send('shutdown')
                forkAlternative(i)
            }
        }).listen('server.sock')

        process.on('SIGINT', shutdown)
        process.on('SIGTERM', shutdown)

    })

}
