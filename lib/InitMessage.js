var fs = require('fs')

module.exports = logsDir => {

    var config = require('../config.js')

    var httpsOptions = config.https
    if (httpsOptions) {

        httpsOptions.key = fs.readFileSync(httpsOptions.key, 'utf8')
        httpsOptions.cert = fs.readFileSync(httpsOptions.cert, 'utf8')

        var ca = httpsOptions.ca
        for (var i in ca) ca[i] = fs.readFileSync(ca[i], 'utf8')
        httpsOptions.ca = ca

    }

    var port = config.port
    if (!port) port = 3128

    var timeout = config.timeout || 60000

    return {
        host: config.host,
        port: port,
        timeout: timeout,
        logsDir: logsDir,
        httpsOptions: httpsOptions,
    }

}
