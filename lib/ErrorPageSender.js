var ErrorPage = require('./ErrorPage.js')

var headers = { 'Content-Type': 'text/html; charset=UTF-8' }

module.exports = (statusCode, logFile) => {
    var html = ErrorPage(statusCode)
    return (res, logLine) => {

        logLine.setResponse(statusCode, headers)
        logFile.add(logLine)

        res.writeHead(statusCode, headers)
        res.end(html)

    }
}
