const http = require('http')

module.exports = headers => {
    for (const i in headers) {
        try {
            http.validateHeaderName(i)
            http.validateHeaderValue(i, headers[i])
        } catch (e) {
            if (!(e instanceof TypeError)) throw e
            delete headers[i]
        }
    }
}
