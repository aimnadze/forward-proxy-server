var createServer = require('./createServer.js')

module.exports = (httpsOptions, port, host, callback) => {
    var server = createServer(httpsOptions)
    server.listen(port, host, () => {
        server.close(callback)
    })
    server.on('error', err => {
        var code = err.code
        if (code == 'EACCES') {
            console.error('ERROR: permission denied')
        } else if (code == 'EADDRINUSE') {
            console.error('ERROR: address in use')
        } else if (code == 'EADDRNOTAVAIL') {
            console.error('ERROR: address not available')
        } else {
            throw err
        }
        process.exit(1)
    })
}
