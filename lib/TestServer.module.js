module.exports = app => (initMessage, port, host, callback) => {

    const log = app.log

    const server = app.CreateServer(initMessage)
    server.listen(port, host, () => {
        server.close(callback)
    })
    server.on('error', err => {
        const code = err.code
        if (code === 'EACCES') {
            log.error('permission denied')
        } else if (code === 'EADDRINUSE') {
            log.error('address in use')
        } else if (code === 'EADDRNOTAVAIL') {
            log.error('address not available')
        } else {
            throw err
        }
        process.exit(1)
    })

}
