var http = require('http'),
    ErrorPage = require('./ErrorPage.js')

module.exports = (statusCode, logFile) => {

    var headers = { 'Content-Type': 'text/html; charset=UTF-8' }

    var title = http.STATUS_CODES[statusCode]
    var response =
        statusCode + ' ' + title + '\r\n' +
        'Content-Type: text/html; charset=UTF-8\r\n' +
        '\r\n' +
        ErrorPage(400)

    return (socket, logLine) => {

        logLine.setResponse(statusCode, headers)
        logFile.add(logLine)

        socket.end(response)

    }

}
