module.exports = httpsOptions => {
    if (httpsOptions) return require('https').createServer(httpsOptions)
    return require('http').createServer()
}
