const headers = { 'Content-Type': 'text/html; charset=UTF-8' }

module.exports = app => (statusCode, logFile) => {
    const html = app.ErrorPage(statusCode)
    return (res, logLine) => {

        logLine.setResponse(statusCode, headers)
        logFile.add(logLine)

        res.writeHead(statusCode, headers)
        res.end(html)

    }
}
