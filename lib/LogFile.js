var crypto = require('crypto'),
    fs = require('fs'),
    LogLine = require('./LogLine.js')

module.exports = (logsDir, onRelease) => {

    function release () {
        var fileToRelease = file
        stream.end(() => {
            onRelease(fileToRelease)
        })
    }

    function releaseAndReset () {
        release()
        reset()
    }

    function reset () {
        file = logsDir + '/' + crypto.randomBytes(8).toString('hex')
        stream = fs.createWriteStream(file)
        numLines = 0
        timeout = setTimeout(releaseAndReset, 1000 * 60 * 60 * 24)
    }

    function shutdown () {
        clearTimeout(timeout)
        release()
    }

    var file, stream, numLines, timeout
    var shuttingDown = false
    var numPendingLines = 0

    reset()

    return {
        add: logLine => {
            stream.write(logLine.toJsonLine())
            numLines++
            numPendingLines--
            if (shuttingDown && !numPendingLines) {
                shutdown()
            } else if (numLines == 1024 * 32) {
                clearTimeout(timeout)
                releaseAndReset()
            }
        },
        newLine: req => {
            numPendingLines++
            return LogLine(req)
        },
        shutdown: () => {
            shuttingDown = true
            if (!numPendingLines) shutdown()
        },
    }

}
