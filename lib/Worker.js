var fixHeaders = require('./fixHeaders.js')

module.exports = () => {
    process.once('message', initMessage => {

        var http = require('http'),
            net = require('net'),
            url = require('url'),
            createServer = require('./createServer.js'),
            ErrorPage = require('./ErrorPage.js'),
            ErrorPageSender = require('./ErrorPageSender.js'),
            ErrorPageWriter = require('./ErrorPageWriter.js'),
            LogFile = require('./LogFile.js')

        var logFile = LogFile(initMessage.logsDir, logFile => {
            process.send(logFile)
        })

        var error400Writer = ErrorPageWriter(400, logFile),
            error502Writer = ErrorPageWriter(502, logFile)

        var error400Sender = ErrorPageSender(400, logFile),
            error404Sender = ErrorPageSender(404, logFile),
            error502Sender = ErrorPageSender(502, logFile),
            error504Sender = ErrorPageSender(504, logFile)

        var server = createServer(initMessage.httpsOptions)
        server.listen(initMessage.port, initMessage.host)
        server.on('request', (req, res) => {

            function sendBadGateway () {
                error502Sender(res, logLine)
            }

            var logLine = logFile.newLine(req)

            var parsedUrl = url.parse(req.url)

            if (parsedUrl.host === null) {
                error404Sender(res, logLine)
                return
            }

            if (parsedUrl.protocol === 'http:') {

                var headers = req.headers
                fixHeaders(headers)

                var options = {
                    host: parsedUrl.hostname,
                    port: parsedUrl.port,
                    method: req.method,
                    path: parsedUrl.path,
                    headers: headers,
                }

                var proxyReq = http.request(options, proxyRes => {

                    clearTimeout(abortTimeout)

                    var statusCode = proxyRes.statusCode,
                        headers = proxyRes.headers

                    logLine.setResponse(statusCode, headers)
                    logFile.add(logLine)

                    proxyReq.removeListener('error', sendBadGateway)
                    proxyReq.on('error', () => {
                        res.end()
                    })

                    res.writeHead(statusCode, headers)
                    proxyRes.pipe(res)

                })
                proxyReq.on('error', sendBadGateway)

                req.pipe(proxyReq)
                res.on('error', () => {
                    proxyReq.removeListener('error', sendBadGateway)
                    proxyReq.abort()
                })

                var abortTimeout = setTimeout(() => {
                    proxyReq.removeListener('error', sendBadGateway)
                    proxyReq.on('error', () => {
                        error504Sender(res, logLine)
                    })
                    proxyReq.abort()
                }, initMessage.timeout)

                return

            }

            error400Sender(res, logLine)

        })
        server.on('connect', (req, socket) => {

            function sendBadGateway () {
                error502Writer(socket, logLine)
            }

            var logLine = logFile.newLine(req)

            var parsedUrl = url.parse('http://' + req.url)

            var port = parsedUrl.port
            if (port === null) {
                error400Writer(socket, logLine)
                return
            }

            var proxySocket = net.connect(port, parsedUrl.hostname, () => {

                proxySocket.removeListener('error', sendBadGateway)
                proxySocket.on('error', () => {
                    socket.end()
                })

                logLine.setResponse(200, {})
                logFile.add(logLine)

                socket.write('200 OK\r\n\r\n')
                socket.pipe(proxySocket)
                proxySocket.pipe(socket)

            })
            proxySocket.on('error', sendBadGateway)

        })

        process.once('message', () => {
            server.close()
            logFile.shutdown()
        })

    })
}
