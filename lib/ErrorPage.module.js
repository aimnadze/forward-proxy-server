const http = require('http')

module.exports = app => statusCode => {
    const title = statusCode + ' ' + http.STATUS_CODES[statusCode]
    return app.HtmlPage(
        '<title>' + title + '</title>',
        '<h1>' + title + '</h1>'
    )
}
