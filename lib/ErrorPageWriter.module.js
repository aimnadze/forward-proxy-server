const http = require('http')

const headers = { 'Content-Type': 'text/html; charset=UTF-8' }

module.exports = app => (statusCode, logFile) => {

    const title = http.STATUS_CODES[statusCode]
    const response =
        statusCode + ' ' + title + '\r\n' +
        'Content-Type: text/html; charset=UTF-8\r\n' +
        '\r\n' +
        app.ErrorPage(400)

    return (socket, logLine) => {

        logLine.setResponse(statusCode, headers)
        logFile.add(logLine)

        socket.end(response)

    }

}
