const http = require('http')
const net = require('net')
const url = require('url')

module.exports = app => () => {

    const initMessage = app.InitMessage()

    const listen = app.config.listen

    const logFile = app.LogFile(logFile => {
        process.send(logFile)
    })

    const error400Writer = app.ErrorPageWriter(400, logFile)
    const error502Writer = app.ErrorPageWriter(502, logFile)

    const error400Sender = app.ErrorPageSender(400, logFile)
    const error404Sender = app.ErrorPageSender(404, logFile)
    const error502Sender = app.ErrorPageSender(502, logFile)
    const error504Sender = app.ErrorPageSender(504, logFile)

    const server = app.CreateServer(initMessage)
    server.listen(listen.port, listen.host)
    server.on('request', (req, res) => {

        function sendBadGateway () {
            error502Sender(res, logLine)
        }

        const logLine = logFile.newLine(req)

        const parsedUrl = url.parse(req.url)
        if (parsedUrl.host === null) {
            error404Sender(res, logLine)
            return
        }

        if (parsedUrl.protocol === 'http:') {

            const headers = req.headers
            app.FixHeaders(headers)

            const options = {
                host: parsedUrl.hostname,
                port: parsedUrl.port,
                method: req.method,
                path: parsedUrl.path,
                headers: headers,
            }

            const proxyReq = http.request(options, proxyRes => {

                abort_timeout()

                const statusCode = proxyRes.statusCode,
                    headers = proxyRes.headers

                logLine.setResponse(statusCode, headers)
                logFile.add(logLine)

                proxyReq.removeListener('error', sendBadGateway)
                proxyReq.on('error', () => {
                    res.end()
                })

                res.writeHead(statusCode, headers)
                proxyRes.pipe(res)

            })
            proxyReq.on('error', sendBadGateway)

            req.pipe(proxyReq)
            res.on('error', () => {
                proxyReq.removeListener('error', sendBadGateway)
                proxyReq.abort()
            })

            const abort_timeout = app.Timeout(() => {
                proxyReq.removeListener('error', sendBadGateway)
                proxyReq.on('error', () => {
                    error504Sender(res, logLine)
                })
                proxyReq.abort()
            }, initMessage.timeout)

            return

        }

        error400Sender(res, logLine)

    })
    server.on('connect', (req, socket) => {

        function sendBadGateway () {
            error502Writer(socket, logLine)
        }

        const logLine = logFile.newLine(req)

        const parsedUrl = url.parse('http://' + req.url)

        const port = parsedUrl.port
        if (port === null) {
            error400Writer(socket, logLine)
            return
        }

        const proxySocket = net.connect(port, parsedUrl.hostname, () => {

            proxySocket.removeListener('error', sendBadGateway)
            proxySocket.on('error', () => {
                socket.end()
            })

            logLine.setResponse(200, {})
            logFile.add(logLine)

            socket.write('200 OK\r\n\r\n')
            socket.pipe(proxySocket)
            proxySocket.pipe(socket)

        })
        proxySocket.on('error', sendBadGateway)

    })

}
