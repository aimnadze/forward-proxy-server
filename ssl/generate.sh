#!/bin/bash
cd `dirname $BASH_SOURCE`
openssl req -x509 -nodes -days 2190 -newkey rsa:2048 -keyout key -out cert
