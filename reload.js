#!/usr/bin/env node
process.chdir(__dirname)
var net = require('net')
var client = net.createConnection('server.sock', () => {
    client.end()
})
client.on('error', err => {
    var code = err.code
    if (code == 'ENOENT') console.error('ERROR: server not started')
    else if (code == 'EACCES') console.error('ERROR: permission denied')
    else throw err
})
